import React, { Component } from "react";
import _ from "lodash";
import Container from "react-bootstrap/Container";
import Form from "react-bootstrap/Form";
import Col from "react-bootstrap/Col";
import Row from "react-bootstrap/Row";
import Button from "react-bootstrap/Button";
import "./css/Forms.css";

class SignUp extends Component {
  constructor(props) {
    super(props);
    this.state = {
      username: "",
      password: ""
    };
  }

  _handleUsernameChange = event => {
    this.setState({ username: event.target.value });
  };

  _handlePasswordChange = event => {
    this.setState({ password: event.target.value });
  };

  _handleFormSubmit = event => {
    const { username, password } = this.state;
    const { isSignUpSuccessful } = this.props;
    event.preventDefault();
    this.props.userRequestRegister(username, password);
    console.log(username, password);
  };

  _handleUserLoggout() {
    if (window.confirm("Are you sure you wish to logout from system?")) {
      this.props.userRequestLogout();
    }
    //console.log("User logged out ");
  }

  render() {
    const { username, password } = this.state;
    const isFormValid =
      _.isEmpty(username) | _.isEmpty(password) ? true : false;
    return (
      <Container>
        <Row className='form-card'>
          <Col md={{ span: 4, offset: 4 }}>
            <Form onSubmit={this._handleFormSubmit}>
              <Form.Group controlId='formGroupEmail'>
                <Form.Label>Email address</Form.Label>
                <Form.Control
                  type='email'
                  placeholder='Enter email'
                  onChange={this._handleUsernameChange}
                />
              </Form.Group>
              <Form.Group controlId='formGroupPassword'>
                <Form.Label>Password</Form.Label>
                <Form.Control
                  type='password'
                  placeholder='Password'
                  onChange={this._handlePasswordChange}
                />
              </Form.Group>
              <Button
                className='submit-btn'
                variant='primary'
                type='submit'
                disabled={isFormValid}
              >
                Sign up
              </Button>
            </Form>
          </Col>
          <Col>
            <Button
              className='submit-btn'
              variant='primary'
              type='button'
              onClick={this._handleUserLoggout.bind(this)}
            >
              Log out
            </Button>
          </Col>
        </Row>
      </Container>
    );
  }
}

export { SignUp };
