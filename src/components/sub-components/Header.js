import React, { Component } from 'react';
import {
    Collapse,
    Navbar,
    NavbarToggler,
    NavbarBrand,
    Nav,
    NavItem,
    NavLink,
    NavbarText,
    Button,
} from 'reactstrap';

class Header extends Component {
    constructor(props) {
        super(props);
        this.state = {
            isOpen: false,
        };
    }
    _toggle() {
        const { isOpen } = this.state;
        this.setState({ isOpen: !isOpen });
    }

    _handleLogout() {
        const { userRequestLogout } = this.props;
        if (window.confirm('Are you sure you wish to logout from system?')) {
            userRequestLogout();
        }
    }

    render() {
        const { isOpen } = this.state;
        return (
            <div>
                <Navbar color="light" light expand="md">
                    <NavbarBrand href="/">Stock</NavbarBrand>
                    <NavbarToggler onClick={this._toggle.bind(this)} />
                    <Collapse isOpen={isOpen} navbar>
                        <Nav className="mr-auto" navbar>
                            <NavItem>
                                <NavLink href="/components/">
                                    Product Stats
                                </NavLink>
                            </NavItem>
                        </Nav>
                        <NavbarText>
                            <Button
                                variant="warning"
                                onClick={this._handleLogout.bind(this)}
                            >
                                Logout
                            </Button>
                        </NavbarText>
                    </Collapse>
                </Navbar>
            </div>
        );
    }
}
export default Header;
