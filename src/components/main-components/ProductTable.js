import React, { Component } from 'react';
import _ from 'lodash';
import DatePicker from 'react-datepicker';
import './css/StudentRegister.css';
import { BootstrapTable, TableHeaderColumn } from 'react-bootstrap-table';
import 'react-datepicker/dist/react-datepicker.css';

class ProductTable extends Component {
    constructor(props) {
        super(props);
        this.state = {
            selectedRow: [],
            file: '',
            imagePreviewUrl: '',
            date: new Date(),
            productName: '',
            productPrice: '',
            productAmount: '',
            productDescription: '',
            productType: '',
        };
        this._dateChanged = this._dateChanged.bind(this);
    }

    componentDidMount() {
        const { loadProducts } = this.props;
        loadProducts();
    }

    componentDidUpdate(preProducts) {
        if (
            !_.isEqual(this.props.preProducts, preProducts.products) ||
            !_.isEqual(preProducts.products, this.props.products)
        ) {
            this.props.loadProducts();
            console.log('Pre pro ', preProducts.products);
            console.log('update pro ', this.props.products);
        }
    }

    _handleRegistrationSubmit = e => {
        e.preventDefault();
        const {
            imagePreviewUrl,
            productAmount,
            productDescription,
            productName,
            productPrice,
            productType,
            date,
        } = this.state;
        const { products } = this.props;
        const productData = _.toArray(products);
        const id = _.size(productData) + 1;
        const product = _.assign({
            imagePreviewUrl: imagePreviewUrl,
            id: id,
            date: date,
            productName: productName,
            productAmount: productAmount,
            productPrice: productPrice,
            productDescription: productDescription,
            productType: productType,
        });
        console.log('dsds', date);
        this.props.addProduct(product);
    };

    _customizeModalHeader(onClose, onSave) {
        const headerStyle = {
            backgroundColor: '#50b6bb',
        };
        return (
            <div className="modal-header" style={headerStyle}>
                <h5>ADD NEW CARD</h5>
            </div>
        );
    }

    _customizeModalFooter = (onClose, onSave) => {
        const style = {
            backgroundColor: '#ffffff',
        };
        return (
            <div className="modal-footer" style={style}>
                <button className="btn btn-xs btn-secondary" onClick={onClose}>
                    Close
                </button>
                <button className="btn btn-xs btn-info" onClick={onSave}>
                    Save
                </button>
            </div>
        );
    };

    _renderPaginationPanel = props => {
        return (
            <div className="container">
                <div className="row">
                    <div className="col">
                        {props.components.sizePerPageDropdown}
                    </div>
                    <div className="col-md-auto">
                        {props.components.pageList}
                    </div>
                </div>
            </div>
        );
    };

    _dateChanged(d) {
        this.setState({ date: d });
        console.log(this.state.date);
    }

    _handleAllInputFieldChange = e => {
        const { name, value } = e.target;
        this.setState({ [name]: value });
        console.log(name, value);
    };
    _handleImageChange(e) {
        e.preventDefault();

        let reader = new FileReader();
        let file = e.target.files[0];

        reader.onloadend = () => {
            this.setState({
                file: file,
                imagePreviewUrl: reader.result,
            });
        };

        reader.readAsDataURL(file);
    }

    _handleProductRemove(id) {
        if (window.confirm('Are you sure you wish to remove this?')) {
            this.props.deleteStudent(id);
            console.log('Removed ? ', id);
        }
    }
    _handleUserLogout() {
        if (window.confirm('Are you sure you wish to logout from system?')) {
            this.props.userRequestLogout();
        }
    }

    _jobNameValidator(value) {
        const response = {
            isValid: true,
            notification: { type: 'success', msg: '', title: '' },
        };
        if (!value) {
            response.isValid = false;
            response.notification.type = 'error';
            response.notification.msg = 'Value must be inserted';
            response.notification.title = 'Requested Value';
        } else if (value.length < 10) {
            response.isValid = false;
            response.notification.type = 'error';
            response.notification.msg = 'Value must have 10+ characters';
            response.notification.title = 'Invalid Value';
        }
        return response;
    }

    _onInsertRow(row) {
        const { addProduct, products } = this.props;
        const id = _.size(products) + 1;
        const product = _.assign({
            imagePreviewUrl: null,
            id: id,
            date: null,
            productName: row.productName,
            productAmount: row.productAmount,
            productPrice: row.productPrice,
            productDescription: row.productDescription,
            productType: row.productType,
        });
        addProduct(product);
    }

    _onDeleteRow(rowKeys, rows) {
        const { deleteProduct } = this.props;
        _.forEach(rowKeys, (val, key) => {
            deleteProduct(val);
        });
    }

    _onSelectRow(row, isSelected) {
        let selectRows = _.cloneDeep(this.state.selectedRow);
        if (!isSelected) {
            selectRows.pop(row);
        } else {
            selectRows.push(row);
        }
        this.setState({
            selectedRow: selectRows,
        });
        console.log(`is selected: ${isSelected}, ${selectRows}`);
    }

    _onSelectAll(isSelected, rows) {
        const selectRows = _.concat(this.state.selectedRow, rows);
        this.setState({
            selectedRow: !isSelected ? [] : selectRows,
        });
        console.log('rows', this.state.selectedRow);
    }

    _onAfterSaveCell(row, cellName, cellValue) {
        console.log(`Save cell ${cellName} with value ${cellValue}`);
        const { updateProduct } = this.props;
        const product = _.assign({
            imagePreviewUrl: null,
            id: row.id,
            date: null,
            productName: row.productName,
            productAmount: row.productAmount,
            productPrice: row.productPrice,
            productDescription: row.productDescription,
            productType: row.productType,
        });
        updateProduct(product, row.id);
    }

    render() {
        const { products } = this.props;
        const selectRowProp = {
            mode: 'checkbox',
            clickToSelect: true,
            bgColor: 'rgb(238, 193, 213)',
            onSelect: this._onSelectRow.bind(this),
            onSelectAll: this._onSelectAll.bind(this),
        };

        const cellEdit = {
            mode: 'dbclick',
            blurToSave: true,
            afterSaveCell: this._onAfterSaveCell.bind(this),
        };
        const options = {
            insertModalHeader: this._customizeModalHeader,
            insertModalFooter: this._customizeModalFooter,
            paginationPanel: this._renderPaginationPanel,
            afterInsertRow: this._onInsertRow.bind(this),
            afterDeleteRow: this._onDeleteRow.bind(this),
        };
        const constructedData = {};
        _.forEach(products, (val, key) => {
            _.merge(constructedData, {
                [key]: {
                    id: key,
                    date: val.date,
                    productName: val.productName,
                    productAmount: val.productAmount,
                    productPrice: val.productPrice,
                    productDescription: val.productDescription,
                    productType: val.productType,
                },
            });
        });
        const dataProducts = _.toArray(constructedData);
        console.log('data product', dataProducts);
        return (
            <div className="register-container">
                <BootstrapTable
                    data={dataProducts}
                    selectRow={selectRowProp}
                    cellEdit={cellEdit}
                    options={options}
                    insertRow
                    deleteRow
                    exportCSV
                    search
                    hover
                    pagination
                    headerStyle={{
                        background: '#50b6bb',
                    }}
                >
                    <TableHeaderColumn
                        dataField="id"
                        isKey
                        dataAlign="center"
                        hidden
                    >
                        ID
                    </TableHeaderColumn>

                    <TableHeaderColumn
                        dataField="productName"
                        width="175"
                        dataAlign="center"
                        editable={{
                            type: 'textarea',
                            validator: this._jobNameValidator,
                        }}
                    >
                        Name
                    </TableHeaderColumn>
                    <TableHeaderColumn
                        dataField="productType"
                        width="175"
                        editable={{
                            type: 'select',
                            options: {
                                values: [
                                    'Beverage',
                                    'Food',
                                    'Alcohol',
                                    'Unknown',
                                ],
                            },
                        }}
                    >
                        Type
                    </TableHeaderColumn>
                    <TableHeaderColumn
                        dataField="productPrice"
                        width="175"
                        dataSort
                    >
                        Price
                    </TableHeaderColumn>
                    <TableHeaderColumn dataField="productAmount" width="175">
                        Amount
                    </TableHeaderColumn>
                    <TableHeaderColumn
                        dataField="productDescription"
                        width="175"
                    >
                        Description
                    </TableHeaderColumn>
                </BootstrapTable>
            </div>
        );
    }
}

export { ProductTable };
