import firebase from '../utils/Firebase';
import _ from 'lodash';
import {
    ADD_PRODUCT,
    READ_PRODUCT,
    UPDATE_PRODUCT,
    DELETE_PRODUCT,
} from './ActionList';

export const addProduct = (product: Object) => {
    return async (dispatch: Function, getState: Function) => {
        try {
            await firebase
                .database()
                .ref(`products`)
                .push(product)
                .then(() => {
                    console.log('Register successfully: ', product);
                    alert('Added successfully: ', product);
                });
            dispatch({
                type: ADD_PRODUCT,
                productId: product.id,
            });
        } catch (error) {
            console.warn(error);
        }
    };
};

export const loadProducts = () => {
    return async (dispatch: Function, getState: Function) => {
        try {
            await firebase
                .database()
                .ref(`products`)
                .once('value', snapshot => {
                    const products = snapshot.val();
                    console.log('actions loading', products);
                    dispatch({
                        type: READ_PRODUCT,
                        products: products,
                    });
                })
                .catch(error => {
                    console.warn(error);
                });
        } catch (error) {
            console.warn(error);
        }
    };
};

export const updateProduct = (product: Object, productId: String) => {
    return async (dispatch: Function, getState: Function) => {
        try {
            await firebase
                .database()
                .ref(`products/${productId}`)
                .update(product)
                .then(() => {
                    dispatch({
                        type: UPDATE_PRODUCT,
                        productId: productId,
                    });
                })
                .catch(error => {
                    console.warn(error);
                });
        } catch (error) {
            console.warn(error);
        }
    };
};

export const deleteProduct = (productId: String) => {
    return async (dispatch: Function, getState: Function) => {
        try {
            await firebase
                .database()
                .ref(`products/${productId}`)
                .remove()
                .then(() => {
                    console.log('Successfully Removed', productId);
                    alert('Successfully Removed', productId);
                    dispatch({
                        type: DELETE_PRODUCT,
                        productId: productId,
                    });
                })
                .catch(error => {
                    console.warn(error);
                });
        } catch (error) {
            console.warn(error);
        }
    };
};
