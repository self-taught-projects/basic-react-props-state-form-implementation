// User authentication
export const USER_REQUEST_LOGIN = 'USER_REQUEST_LOGIN';
export const USER_REQUEST_LOGOUT = 'USER_REQUEST_LOGOUT';
export const USER_REQUEST_SIGNUP = 'USER_REQUEST_SIGNUP';

// Product constant
export const ADD_PRODUCT = 'ADD_PRODUCT';
export const DELETE_PRODUCT = 'ADD_PRODUCT';
export const UPDATE_PRODUCT = 'ADD_PRODUCT';
export const READ_PRODUCT = 'ADD_PRODUCT';
