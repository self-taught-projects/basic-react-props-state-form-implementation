import firebase from '../utils/Firebase';
import {
    USER_REQUEST_LOGIN,
    USER_REQUEST_LOGOUT,
    USER_REQUEST_SIGNUP,
} from './ActionList';

export const userRequestLoginWithEmailAndPassword = (
    email: string,
    password: string
) => {
    return async (dispatch: Function, getState: Function) => {
        try {
            await firebase
                .auth()
                .signInWithEmailAndPassword(email, password)
                .then(result => {
                    dispatch({
                        type: USER_REQUEST_LOGIN,
                        userId: result.user.userId,
                    });
                })
                .catch(error => {
                    console.warn(error);
                    alert('Email or password might be incorrect !');
                });
        } catch (error) {
            console.warn(error);
            alert('Email or password might be incorrect !');
        }
    };
};

export const userRequestLogout = () => {
    return async (dispatch: Function, getState: Function) => {
        try {
            await firebase
                .auth()
                .signOut()
                .then(() => {
                    dispatch({ type: USER_REQUEST_LOGOUT });
                })
                .catch(error => {
                    console.warn(error);
                });
        } catch (error) {
            console.warn(error);
        }
    };
};

export const userRequestRegister = (email: string, password: string) => {
    return async (dispatch: Function, getState: Function) => {
        try {
            await firebase
                .auth()
                .createUserWithEmailAndPassword(email, password)
                .then(result => {
                    dispatch({
                        type: USER_REQUEST_SIGNUP,
                        userId: result.user.userId,
                    });
                    alert('Created Account Successfully !');
                })
                .catch(error => {
                    console.warn(error);
                });
        } catch (error) {
            console.warn(error);
        }
    };
};
