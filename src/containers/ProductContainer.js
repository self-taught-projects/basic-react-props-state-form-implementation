import { connect } from 'react-redux';
import _ from 'lodash';
import { userRequestLogout } from '../actions/UserAction';
import { ProductTable } from '../components/main-components/ProductTable';
import {
    addProduct,
    loadProducts,
    deleteProduct,
    updateProduct,
} from '../actions/ProductAction';

const mapStateToProps = state => {
    const { product } = state;
    const {
        products,
        updateProducts,
        isRegisterSuccess,
        isModalShowing,
    } = product;
    console.log('Up', updateProducts);
    return {
        isRegisterSuccess,
        products,
        isModalShowing,
    };
};
const mapDispatchToProps = (dispatch: Function) => {
    return {
        addProduct: (product: Object) => {
            dispatch(addProduct(product));
        },
        loadProducts: () => {
            dispatch(loadProducts());
        },
        deleteProduct: (id: String) => {
            dispatch(deleteProduct(id));
        },
        updateProduct: (product: Object, productId: String) => {
            dispatch(updateProduct(product, productId));
        },
        userRequestLogout: () => {
            dispatch(userRequestLogout());
        },
    };
};
export default connect(mapStateToProps, mapDispatchToProps)(ProductTable);
