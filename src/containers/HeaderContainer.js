import { connect } from 'react-redux';
import Header from '../components/sub-components/Header';
import { userRequestLogout } from '../actions/UserAction';

const mapDispatchToProps = (dispatch: Function) => {
    return {
        userRequestLogout: () => {
            dispatch(userRequestLogout());
        },
    };
};

export default connect(null, mapDispatchToProps)(Header);
