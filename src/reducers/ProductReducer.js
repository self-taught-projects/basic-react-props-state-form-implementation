import {
    ADD_PRODUCT,
    UPDATE_PRODUCT,
    DELETE_PRODUCT,
    READ_PRODUCT,
} from '../actions/ActionList';
import _ from 'lodash';

const INITIALIZED_STATE = {
    isRegisterSuccess: false,
    updatedStudent: {},
    products: {},
    updateProdct: {},
    productId: '',
    isModalShowing: false,
    foods: {},
};

const loadFoods = (state, action) => {
    return {
        ...state,
        foods: action.foods,
    };
};
const loadProducts = (state, action) => {
    return {
        ...state,
        products: action.products,
    };
};

const deleteProduct = (state, action) => {
    return {
        ...state,
        productId: action.productId,
    };
};

const updateProdct = (state, action) => {
    return {
        ...state,
        productId: action.productId,
    };
};

const addProduct = (state, action) => {
    return {
        ...state,
        products: action.products,
    };
};

export default (state = INITIALIZED_STATE, action) => {
    switch (action.type) {
        case ADD_PRODUCT:
            return addProduct(state, action);
        case UPDATE_PRODUCT:
            return updateProdct(state, action);
        case DELETE_PRODUCT:
            return deleteProduct(state, action);
        case READ_PRODUCT:
            return loadProducts(state, action);
        default:
            return state;
    }
};
