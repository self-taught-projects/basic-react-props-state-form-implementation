import UserReducer from "./UserReducer";
import SignUpReducer from "./SignUpReducer";
import ProductReducer from "./ProductReducer";

const RootReducer = {
  user: UserReducer,
  signup: SignUpReducer,
  product: ProductReducer
};

export default RootReducer;
