import { USER_REQUEST_LOGIN, USER_REQUEST_LOGOUT } from '../actions/ActionList';

const INITIALIZED_STATE = {
    isUserLoggedIn: false,
    userId: '',
};

const userRequestLogin = (state, action) => {
    return {
        ...state,
        isUserLoggedIn: true,
        userId: action.userId,
    };
};

const userRequestLogout = state => {
    return {
        ...state,
        isUserLoggedIn: false,
        userId: '',
    };
};

export default (state = INITIALIZED_STATE, action) => {
    switch (action.type) {
        case USER_REQUEST_LOGIN:
            return userRequestLogin(state, action);
        case USER_REQUEST_LOGOUT:
            return userRequestLogout(state);
        default:
            return state;
    }
};
