import React, { Component } from "react";
import { PersistGate } from "redux-persist/integration/react";
import { Provider } from "react-redux";
import { configStore } from "../src/system/ConfigStore";
import RootContainer from "./system/RootContainer";
import 'bootstrap/dist/css/bootstrap.min.css';
const { store, persistor } = configStore();

class App extends Component<{}> {
  render() {
    return (
      <Provider store={store}>
        <PersistGate loading={null} persistor={persistor}>
          <RootContainer />
        </PersistGate>
      </Provider>
    );
  }
}

export default App;
