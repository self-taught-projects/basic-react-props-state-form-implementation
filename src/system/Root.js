import React, { Component } from 'react';
import { BrowserRouter as Router, Route, Switch } from 'react-router-dom';
import AuthContainer from '../containers/AuthContainer';
import HeaderContainer from '../containers/HeaderContainer';
import ProductContainer from '../containers/ProductContainer';

class Root extends Component {
    render() {
        return (
            <Router>
                {this.props.isUserLoggedIn ? <HeaderContainer /> : null}
                <Switch>
                    <Route
                        exact
                        path="/*"
                        render={() =>
                            !this.props.isUserLoggedIn ? (
                                <AuthContainer />
                            ) : (
                                <ProductContainer />
                            )
                        }
                    />
                    <Route
                        path="/product"
                        render={() =>
                            !this.props.isUserLoggedIn ? (
                                <AuthContainer />
                            ) : (
                                <ProductContainer />
                            )
                        }
                    />
                </Switch>
            </Router>
        );
    }
}

export default Root;
