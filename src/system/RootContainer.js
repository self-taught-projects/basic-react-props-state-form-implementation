import { connect } from 'react-redux';
import Root from './Root';

const mapStateToProps = state => {
    const { user } = state;
    const { isUserLoggedIn } = user;
    console.log('State login', isUserLoggedIn);

    return {
        isUserLoggedIn,
    };
};

export default connect(mapStateToProps, null)(Root);
