import storage from "redux-persist/lib/storage";
import {
  persistStore,
  persistReducer,
  persistCombineReducers
} from "redux-persist";
import RootReducer from "../reducers/RootReducer";
import thunk from "redux-thunk";
import logger from "redux-logger";
import { applyMiddleware, createStore } from "redux";

export const configStore = () => {
  const persistConfig = {
    key: "root",
    storage,
    whilelist: ["user", "signup", "product"]
  };
  const persistedReducer = persistCombineReducers(persistConfig, RootReducer);
  const middleware = [thunk];
  middleware.push(logger);
  const store = createStore(persistedReducer, applyMiddleware(...middleware));
  const persistor = persistStore(store);
  return {
    store,
    persistor
  };
};
